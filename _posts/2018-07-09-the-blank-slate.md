---
layout: post
title: The Blank Slate
date: 2018-07-09
categories: misc
---

The blank page is an intimidating place, but for some reason I find myself there quite often, especially when it comes to my online presence. I think I wipe the slate clean because my interest wanes over time and I begin focusing on other things. What I really need, is the central hub of all things Wes. All of my interests, all of my projects, my health goals, etc. So here's my next stab at it.