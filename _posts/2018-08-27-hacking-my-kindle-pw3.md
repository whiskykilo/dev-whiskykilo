---
layout: post
title: Hacking My Kindle Paperwhite 3
date: 2018-08-27
categories: hacking
---

My Kindle Paperwhite 3 has firmware version 5.8.11, which by most websites is said to be un-jailbreakable, but it really was rather simple. Below are the steps I went through to jailbreak mine:

1. Connect Kindle to computer over USB
2. Drag `update_PW3_5.7.4_initial.bin` onto the mounted Kindle storage root
3. Settings > Device > Update Your Kindle
4. Wait - It takes a long freaking time and will stay at the kindle screen with the tree for an eternity
5. Drag `main-htmlviewer.tar.gz` onto the mounted Kindle storage root
6. On the home page of your kindle type ;installHtml into the search bar, then hit return
7. When the Kindle reboots, you should see a new thing in your library that says: You are Jailbroken
8. Drag `Update_jailbreak_hotfix_1.14.N_install.bin` onto the mounted Kindle storage root

* MD5 of `update_PW3_5.7.4_initial.bin`: 14f2267189b9ecc51d89e4e86f201e75
* MD5 of `main-htmlviewer.tar.gz`: 8d4ef0528bc1d72576b890a72840780a
* MD5 of `Update_jailbreak_hotfix_1.14.N_install.bin`: d0bcbe44077b0b83898b765791840221

Update: My Kindle now has much worse battery life than before. I will be digging into this to investigate.